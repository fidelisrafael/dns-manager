# DNS Manager

<div style="background-color: white; text-align: center; border-radius: 20px; padding: 10px;">

![Logo DNS Manager](./public/dns-logo.png)

</div>

---

## Master (Production):

[![pipeline status](https://gitlab.com/fidelisrafael/dns-manager/badges/master/pipeline.svg)](https://gitlab.com/fidelisrafael/dns-manager/-/pipelines?page=1&scope=all&ref=master)

[![coverage report](https://gitlab.com/fidelisrafael/dns-manager/badges/master/coverage.svg)](https://gitlab.com/fidelisrafael/dns-manager/-/tree/master)

## Develop (Homolog):

[![pipeline status](https://gitlab.com/fidelisrafael/dns-manager/badges/develop/pipeline.svg)](https://gitlab.com/fidelisrafael/dns-manager/-/pipelines?page=1&scope=all&ref=develop)

[![coverage report](https://gitlab.com/fidelisrafael/dns-manager/badges/develop/coverage.svg)](https://gitlab.com/fidelisrafael/dns-manager/-/tree/master)

---

## Setup without Docker

Install Ruby 2.7.1 through [`rbenv`](https://github.com/rbenv/rbenv) ou or [RVM](http://rvm.io/).

For this project we will be using `rbenv`

```sh
$ git clone git@gitlab.com:fidelisrafael/dns-manager.git
$ cd dns-manager/
$ rbenv install 2.7.1
$ rbenv local 2.7.1
$ ruby -v  # ruby 2.7.1p83 (2020-03-31 revision a0c7c23c9c) [x86_64-linux]
$ gem install bundler --no-doc
$ bundle install
```

If you are facing problems installing the `pg` gem, make sure that you have PostgreSQL + PostgreSQL-devel installed on your machine so Rubygems can build this native extension gem. Below some commands to install it on **OpenSuse** and **Fedora**:

**OpenSuse**:

```sh
$ sudo zypper install postgresql12 postgresql12-devel postgresql-server-devel
```

**Fedora**:

```sh
$ sudo dnf install postgresql-server postgresql-contrib libpq libpq-devl
```

## PostgreSQL Database

For a rapid development setup you can create a database with the credentials already set in `config/database.yml` in your `development` environment.

The default configuration is:

```yaml
development:
  <<: *default
  database: dns_manager_development
  username: postgres
  password: ad3225ca3430a25e1e2a8bb6d97efa79
  host: 127.0.0.1
  port: 5432
```

### Setup: With Docker :)

The simplest way to run a PostgreSQL server locally it's probably by using the [PostgreSQL image at DockerHub](https://hub.docker.com/_/postgres).

To create a database with the credentials configured in `config/database.yml`, run:

```sh
$ docker run -d --name=dns_manager-dev-postgres -e POSTGRES_DB="dns_manager" -e POSTGRES_PASSWORD="ad3225ca3430a25e1e2a8bb6d97efa79" -e POSTGRES_USER="postgres" -p 5432:5432 postgres:12
```

If everything goes smoothly, check if the container is running with:

```sh
$ docker logs dns_manager-dev-postgres

PostgreSQL init process complete; ready for start up.

2021-02-13 13:42:31.942 UTC [1] LOG:  starting PostgreSQL 12.3 (Debian 12.3-1.pgdg100+1) on x86_64-pc-linux-gnu, compiled by gcc (Debian 8.3.0-6) 8.3.0, 64-bit
2021-02-13 13:42:31.943 UTC [1] LOG:  listening on IPv4 address "0.0.0.0", port 5432
2021-02-13 13:42:31.943 UTC [1] LOG:  listening on IPv6 address "::", port 5432
2021-02-13 13:42:31.989 UTC [1] LOG:  listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
2021-02-13 13:42:32.044 UTC [65] LOG:  database system was shut down at 2021-02-13 13:42:31 UTC
2021-02-13 13:42:32.051 UTC [1] LOG:  database system is ready to accept connections
```

Try to connect into it through the CLI:

```sh
$ docker exec -it dns_manager-dev-postgres psql --host=0.0.0.0 --port=5432 --user="postgres"

psql (12.3 (Debian 12.3-1.pgdg100+1))
Type "help" for help.

postgres=# \l
                                  List of databases
    Name     |  Owner   | Encoding |  Collate   |   Ctype    |   Access privileges
-------------+----------+----------+------------+------------+-----------------------
 dns_manager | postgres | UTF8     | en_US.utf8 | en_US.utf8 |
 postgres    | postgres | UTF8     | en_US.utf8 | en_US.utf8 |
 template0   | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
             |          |          |            |            | postgres=CTc/postgres
 template1   | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
             |          |          |            |            | postgres=CTc/postgres
(4 rows)

postgres=#
```

---

## Run initial migrations

After having the database set up, just run:

```sh
$ bundle exec rake db:setup

Created database 'dns_manager_development'
Created database 'dns_manager_test'
```

--

## Run tests

With the database already setup, you can run the whole test suite with:

```sh
$ bundle exec rspec

# Running:

..............

Finished in 3.29 seconds (files took 2.27 seconds to load)
88 examples, 0 failures

Coverage report generated for RSpec to /(...)/dns-manager/coverage. 154 / 154 LOC (100.0%) covered.
```
