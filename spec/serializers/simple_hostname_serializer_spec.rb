require 'rails_helper'

RSpec.describe SimpleHostnameSerializer, type: :serializer do
  before :each do
    @hostname = create(:hostname)
  end

  def normalize_json(object = {})
    object.as_json.deep_symbolize_keys
  end

  it 'must return the basic representation of Hostname' do
    serializer = described_class.new(@hostname)

    expected = {
      id: @hostname.id,
      label: @hostname.label,
      created_at: @hostname.created_at,
      updated_at: @hostname.updated_at
    }

    expect(normalize_json(serializer.serializable_hash)).to eq(normalize_json(expected))
  end

  it 'must not include root by default' do
    serializer = described_class.new(@hostname)

    expect(serializer.as_json).not_to have_key(:hostname)
    expect(serializer.as_json).not_to have_key(:hostnames)
  end

  it 'must return basic hostname attributes' do
    serializer = described_class.new(@hostname)

    expect(serializer.as_json.keys.sort).to eq(%i[created_at id label updated_at])
  end
end
