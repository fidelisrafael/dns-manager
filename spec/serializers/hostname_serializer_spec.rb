# == Schema Information
#
# Table name: hostnames
#
#  id         :bigint           not null, primary key
#  label      :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_hostnames_on_label  (label) UNIQUE
#
require 'rails_helper'

RSpec.describe HostnameSerializer, type: :serializer do
  before :each do
    @hostname = create(:hostname_with_dns_record)
  end

  def normalize_json(object = {})
    object.as_json.deep_symbolize_keys
  end

  it 'must return the complete representation of Hostname' do
    serializer = described_class.new(@hostname)

    expected = {
      id: @hostname.id,
      label: @hostname.label,
      dns_records: ActiveModel::ArraySerializer.new(@hostname.dns_records, each_serializer: SimpleDnsRecordSerializer).as_json,
      created_at: @hostname.created_at,
      updated_at: @hostname.updated_at
    }

    expect(normalize_json(serializer.serializable_hash)).to eq(normalize_json(expected))
  end

  it 'must not include root by default' do
    serializer = described_class.new(@hostname)

    expect(serializer.as_json).not_to have_key(:hostname)
    expect(serializer.as_json).not_to have_key(:hostnames)
  end

  it 'must return basic Hostname attributes' do
    serializer = described_class.new(@hostname)

    expect(serializer.as_json.keys.sort).to eq(%i[created_at dns_records id label updated_at])
  end

  it 'must inherits from Hostname' do
    expect(described_class.superclass).to eq(SimpleHostnameSerializer)
  end
end
