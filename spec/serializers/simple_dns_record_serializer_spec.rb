require 'rails_helper'

RSpec.describe SimpleDnsRecordSerializer, type: :serializer do
  before :each do
    @dns_record = create(:dns_record)
  end

  def normalize_json(object = {})
    object.as_json.deep_symbolize_keys
  end

  it 'must return the basic representation of dns_record' do
    serializer = described_class.new(@dns_record)

    expected = {
      id: @dns_record.id,
      ip: @dns_record.ip.to_s,
      created_at: @dns_record.created_at,
      updated_at: @dns_record.updated_at
    }

    expect(normalize_json(serializer.serializable_hash)).to eq(normalize_json(expected))
  end

  it 'must not include root by default' do
    serializer = described_class.new(@dns_record)

    expect(serializer.as_json).not_to have_key(:dns_record)
    expect(serializer.as_json).not_to have_key(:dns_records)
  end

  it 'must return basic dns_record attributes' do
    serializer = described_class.new(@dns_record)

    expect(serializer.as_json.keys.sort).to eq(%i[created_at id ip updated_at])
  end
end
