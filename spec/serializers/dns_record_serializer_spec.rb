# == Schema Information
#
# Table name: dns_records
#
#  id         :bigint           not null, primary key
#  ip         :inet             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_dns_records_on_ip  (ip)
#
require 'rails_helper'

RSpec.describe DnsRecordSerializer, type: :serializer do
  before :each do
    @dns_record = create(:dns_record_with_hostname)
  end

  def normalize_json(object = {})
    object.as_json.deep_symbolize_keys
  end

  it 'must return the complete representation of DnsRecord' do
    serializer = described_class.new(@dns_record)

    expected = {
      id: @dns_record.id,
      ip: @dns_record.ip.to_s,
      hostnames: ActiveModel::ArraySerializer.new(@dns_record.hostnames, each_serializer: SimpleHostnameSerializer).as_json,
      created_at: @dns_record.created_at,
      updated_at: @dns_record.updated_at
    }

    expect(normalize_json(serializer.serializable_hash)).to eq(normalize_json(expected))
  end

  it 'must not include root by default' do
    serializer = described_class.new(@dns_record)

    expect(serializer.as_json).not_to have_key(:dns_record)
    expect(serializer.as_json).not_to have_key(:dns_records)
  end

  it 'must return basic DnsRecord attributes' do
    serializer = described_class.new(@dns_record)

    expect(serializer.as_json.keys.sort).to eq(%i[created_at hostnames id ip updated_at])
  end

  it 'must inherits from DnsRecord' do
    expect(described_class.superclass).to eq(SimpleDnsRecordSerializer)
  end
end
