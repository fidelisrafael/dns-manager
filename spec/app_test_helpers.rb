module DNSManager
  module TestsHelpers
    def test_factory
      raise NotImplementedError, "#{__method__} should be implemented in tests using #{self.class}"
    end

    def parsed_response_records(records_key = 'records')
      response.parsed_body[records_key].map { |record| OpenStruct.new(record) }
    end

    def build_model(attrs = {})
      build(test_factory, attrs)
    end

    def create_model_invalidating(attrs = {})
      build_model(attrs).save!(validate: false)
    end

    def test_column_not_null_message(column, column_value = nil)
      expect do
        create_model_invalidating(column => column_value)
      end.to raise_error(/null value in column "#{column}" violates not-null constraint/)
    end

    def test_column_not_null(column, column_value = nil)
      expect do
        create_model_invalidating(column => column_value)
      end.to raise_error(ActiveRecord::NotNullViolation)
    end

    def test_column_not_duplicated_message(column)
      expect do
        create_model_invalidating(column => existent_record[column])
      end.to raise_error(/duplicate key value violates unique constraint "index_#{described_class.table_name}_on_#{column}"/)
    end

    def test_column_not_duplicated(column)
      expect do
        create_model_invalidating(column => existent_record[column])
      end.to raise_error(ActiveRecord::RecordNotUnique)
    end
  end
end
