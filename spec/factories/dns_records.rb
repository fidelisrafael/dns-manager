# == Schema Information
#
# Table name: dns_records
#
#  id         :bigint           not null, primary key
#  ip         :inet             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_dns_records_on_ip  (ip)
#
FactoryBot.define do
  factory :dns_record do
    ip { Faker::Internet.ip_v4_address }
  end

  factory :dns_record_with_hostname, parent: :dns_record do
    hostnames { [create(:hostname)] }
  end
end
