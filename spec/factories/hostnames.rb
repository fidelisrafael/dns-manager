# == Schema Information
#
# Table name: hostnames
#
#  id         :bigint           not null, primary key
#  label      :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_hostnames_on_label  (label) UNIQUE
#
FactoryBot.define do
  factory :hostname do
    label { Faker::Internet.domain_name }
  end

  factory :hostname_with_dns_record, parent: :hostname do
    dns_records { [create(:dns_record)] }
  end
end
