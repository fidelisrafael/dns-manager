# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DnsRecordGetAllService, type: :service do
  let(:options) do
    {
      included: %w[ipsum.com dolor.com],
      excluded: %w[sit.com]
    }
  end

  let(:dns_records_test) do
    [
      # 1.1.1.
      create(:dns_record, ip: '1.1.1.1', hostnames_attributes: [
               { label: 'lorem.com' },
               { label: 'ipsum.com' },
               { label: 'dolor.com' },
               { label: 'amet.com' }
             ]),

      # 2.2.2.2
      create(:dns_record, ip: '2.2.2.2', hostnames_attributes: [
               { label: 'ipsum.com' }
             ]),

      # 3.3.3.3
      create(:dns_record, ip: '3.3.3.3', hostnames_attributes: [
               { label: 'ipsum.com' },
               { label: 'dolor.com' },
               { label: 'amet.com' }
             ]),

      # 4.4.4.4
      create(:dns_record, ip: '4.4.4.4', hostnames_attributes: [
               { label: 'ipsum.com' },
               { label: 'dolor.com' },
               { label: 'sit.com' },
               { label: 'amet.com' }
             ]),

      # 5.5.5.5
      create(:dns_record, ip: '5.5.5.5', hostnames_attributes: [
               { label: 'ipsum.com' },
               { label: 'dolor.com' },
               { label: 'sit.com' },
               { label: 'amet.com' }
             ])
    ]
  end

  it 'must initialize' do
    expect { described_class.new(options) }.not_to raise_error
  end

  it 'must set `response_status = :ok` after executing' do
    service = described_class.new(options)
    service.execute

    expect(service.success?).to be_truthy
    expect(service.response_status).to eq(:ok)
  end

  it 'must set `errors` when execution fails' do
    service = described_class.new({})

    allow(service).to receive(:can_execute_action?).and_return(false)

    service.execute

    expect(service.errors).not_to be_empty
    expect(service.errors).to be_a(Array)
    expect(service.errors.first).to be_a(String)
  end

  it 'must fail when can\'t create a `DnsRecord`' do
    service = described_class.new(options)

    allow(service).to receive(:can_execute_action?).and_return(false)

    service.execute

    expect(service.fail?).to be_truthy
  end

  it 'must set an generic error message when execution fails' do
    service = described_class.new(options)

    allow(service).to receive(:can_execute_action?).and_return(false)

    service.execute

    expect(service.errors).to eq(['translation missing: en.nifty_services.errors.services.cant_execute_get_all_paginated_and_filtered_dns_record'])
  end

  describe 'API' do
    let(:service) do
      described_class.new(options)
    end

    context '#included_hostnames' do
      it {
        expect(service.included_hostnames).to eq(%w[ipsum.com dolor.com])
      }
    end

    context '#excluded_hostnames' do
      it {
        expect(service.excluded_hostnames).to eq(%w[sit.com])
      }
    end

    context '#only_include_filter?' do
      it {
        service = described_class.new(included: %w[ipsum.com dolor.com])

        expect(service.only_include_filter?).to eq(true)
        expect(service.only_exclude_filter?).to eq(false)
        expect(service.all_filters?).to eq(false)
      }
    end

    context '#only_exclude_filter?' do
      it {
        service = described_class.new(excluded: %w[sit.com])

        expect(service.only_exclude_filter?).to eq(true)
        expect(service.only_include_filter?).to eq(false)
        expect(service.all_filters?).to eq(false)
      }
    end

    context '#all_filters?' do
      it {
        service = described_class.new(options)

        expect(service.only_exclude_filter?).to eq(false)
        expect(service.only_include_filter?).to eq(false)
        expect(service.all_filters?).to eq(true)
      }
    end

    context '#related_hostnames_records' do
      it 'should properly match' do
        # Populate database and sanity check
        expect(dns_records_test.size).to eq(5)

        service = described_class.new(options)
        service.execute

        expect(service.related_hostnames_records.size).to eq(3)
        expect(service.related_hostnames_records.map(&:label)).to eq(%w[lorem.com amet.com amet.com])
      end

      describe 'not executed' do
        it 'should return an empty array' do
          # Populate database and sanity check
          expect(dns_records_test.size).to eq(5)

          service = described_class.new(options)

          expect(service.related_hostnames_records).to eq([])
        end
      end
    end

    context '#serialized_related_hostnames_records' do
      it 'should properly serialize' do
        # Populate database and sanity check
        expect(dns_records_test.size).to eq(5)

        service = described_class.new(options)
        service.execute

        expect(service.serialized_related_hostnames_records).to eq([{ hostname: 'lorem.com', count: 1 }, { hostname: 'amet.com', count: 2 }])
      end
    end

    context '#record_error_key' do
      it do
        service = described_class.new(options)

        expect(service.record_error_key).to eq(:services)
      end
    end
  end

  describe 'pagination' do
    it 'should paginate' do
      # Populate database
      _dns_records = create_list(:dns_record_with_hostname, 10)
      service = described_class.new(per_page: 2, page: 2)
      service.execute

      expect(service.records.size).to eq(2)
      expect(service.pagination.items).to eq(2)
      expect(service.pagination.count).to eq(10)
      expect(service.pagination.page).to eq(2)
      expect(service.pagination.pages).to eq(5)
    end

    it 'should paginate using :pagy_params' do
      # Populate database
      _dns_records = create_list(:dns_record_with_hostname, 10)
      service = described_class.new(pagy_params: { per_page: 2, page: 2 })

      service.execute

      expect(service.records.size).to eq(2)
      expect(service.pagination.items).to eq(2)
      expect(service.pagination.count).to eq(10)
      expect(service.pagination.page).to eq(2)
      expect(service.pagination.pages).to eq(5)
    end
  end

  describe 'filters' do
    context 'mixed' do
      it 'should properly filter' do
        # Populate database and sanity check
        expect(dns_records_test.size).to eq(5)

        service = described_class.new(options)
        service.execute

        expect(service.records.map(&:ip).map(&:to_s)).to eq(%w[1.1.1.1 3.3.3.3])
      end
    end

    context 'included' do
      it 'should properly filter' do
        # Populate database and sanity check
        expect(dns_records_test.size).to eq(5)

        service = described_class.new(included: ['ipsum.com', 'amet.com'])
        service.execute

        expect(service.records.map(&:ip).map(&:to_s)).to eq(%w[1.1.1.1 3.3.3.3 4.4.4.4 5.5.5.5])
      end
    end

    context 'excluded' do
      it 'should properly filter' do
        # Populate database and sanity check
        expect(dns_records_test.size).to eq(5)

        service = described_class.new(excluded: ['ipsum.com', 'amet.com'])
        service.execute

        expect(service.records.map(&:ip).map(&:to_s)).to eq(%w[2.2.2.2])
      end
    end
  end
end
