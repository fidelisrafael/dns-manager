# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DnsRecordCreateService, type: :service do
  let(:valid_attributes) { attributes_for(:dns_record) }
  let(:options) { { dns_record: valid_attributes } }

  it 'must initialize' do
    expect { described_class.new(options) }.not_to raise_error
  end

  it 'must set the correct record_type' do
    service = described_class.new(options)

    expect(service.record_type).to be(DnsRecord)
  end

  it 'should properly set whitelisted attributes' do
    service = described_class.new(options)

    expect(service.record_attributes_whitelist).to eq(%i[ip hostnames_attributes])
  end

  it 'must accept DnsRecord data in `@options[:dns_record]`' do
    service = described_class.new(options)

    expect(service.record_attributes_hash).to eq({ ip: valid_attributes[:ip] })
  end

  it 'must accept DnsRecord data in root of `@options`' do
    service = described_class.new(valid_attributes)

    expect(service.record_attributes_hash).to eq({ ip: valid_attributes[:ip] })
  end

  it 'must only accept whitelisted attributes' do
    attributes = valid_attributes.merge(created_at: Time.current)

    service = described_class.new(options.merge(attributes))

    expect(service.record_allowed_attributes).to have_key(:ip)
    expect(service.record_allowed_attributes).not_to have_key(:created_at)
  end

  it 'successfully create a DnsRecord' do
    service = described_class.new(options)

    expect { service.execute }.to change(DnsRecord, :count).by(1)

    expect(service.success?).to be_truthy
  end

  it 'must set `response_status = :created` after creating a DnsRecord' do
    service = described_class.new(options)
    service.execute

    expect(service.success?).to be_truthy
    expect(service.response_status).to eq(:created)
  end

  it 'must set `errors` when execution fails' do
    service = described_class.new(options[:dns_record].merge(ip: 'invalid'))
    service.execute

    expect(service.errors).not_to be_empty
    expect(service.errors).to be_a(Array)
    expect(service.errors.first).to be_a(Hash)
  end

  it 'must fail when user is valid, but can\'t create a `DnsRecord`' do
    service = described_class.new(options)

    allow(service).to receive(:can_create_record?).and_return(false)

    service.execute

    expect(service.fail?).to be_truthy
  end

  it 'must set an generic error message when execution fails' do
    service = described_class.new(options)

    allow(service).to receive(:can_create_record?).and_return(false)

    service.execute

    expect(service.errors).to eq(['translation missing: en.nifty_services.errors.dns_records.cant_create'])
  end

  it 'must fail with empty attributes' do
    service = described_class.new({})
    service.execute

    expect(service.fail?).to be_truthy
    expect(service.errors).not_to be_empty
  end
end
