# == Schema Information
#
# Table name: dns_records
#
#  id         :bigint           not null, primary key
#  ip         :inet             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_dns_records_on_ip  (ip)
#
require 'rails_helper'

RSpec.describe DnsRecord, type: :model do
  alias_method :build_dns_record, :build_model
  alias_method :create_dns_record_invalidating, :create_model_invalidating

  def test_factory
    :dns_record
  end

  describe 'constants' do
    context 'ip_REGEXP' do
      it { expect(DnsRecord::IP_ADDRESS_REGEXP).to eq(Regexp.union(Resolv::IPv4::Regex, Resolv::IPv6::Regex)) }
    end
  end

  describe 'model API' do
    context 'ip' do
      it 'should be a instance of IPAddress' do
        dns_record = build(:dns_record)

        expect(dns_record.ip).to be_a(IPAddr)
      end

      it 'should not allow to set an invalid value' do
        dns_record = build(:dns_record, ip: SecureRandom.hex)

        expect(dns_record.ip).to be_nil
      end

      it 'should allow to use a public IPV4 ip' do
        dns_record = build(:dns_record, ip: '24.29.18.175')

        expect(dns_record).to be_valid
        expect(dns_record.ip).to be_a(IPAddr)
        expect(dns_record.ip.ipv4?).to eq(true)
        expect(dns_record.ip.private?).to eq(false)
      end

      it 'should allow to use a public IPV4 CIDR' do
        dns_record = build(:dns_record, ip: '24.29.18.175/21')

        expect(dns_record).to be_valid
        expect(dns_record.ip).to be_a(IPAddr)
        expect(dns_record.ip.ipv4?).to eq(true)
        expect(dns_record.ip.private?).to eq(false)
      end

      it 'should allow to use a private IPV4 ip' do
        dns_record = build(:dns_record, ip: '10.0.0.1')

        expect(dns_record).to be_valid
        expect(dns_record.ip).to be_a(IPAddr)
        expect(dns_record.ip.ipv4?).to eq(true)
        expect(dns_record.ip.private?).to eq(true)
      end

      it 'should allow to use an IPV6 ip' do
        dns_record = build(:dns_record, ip: 'ac5f:d696:3807:1d72:2eb5:4e81:7d2b:e1df')

        expect(dns_record).to be_valid
        expect(dns_record.ip).to be_a(IPAddr)
        expect(dns_record.ip.ipv6?).to eq(true)
      end

      it 'should allow to use an IPV6 CIDR ip' do
        dns_record = build(:dns_record, ip: 'ac5f:d696:3807:1d72:2eb5:4e81:7d2b:e1df/78')

        expect(dns_record.ip).to be_a(IPAddr)
        expect(dns_record.ip.ipv6?).to eq(true)
      end
    end

    context 'hostnames' do
      it { should accept_nested_attributes_for(:hostnames) }
    end
  end

  describe 'validations' do
    context 'ip' do
      it 'should not accept an invalid ipv4' do
        dns_record = build(:dns_record, ip: 'invalid ip')

        expect(dns_record).to be_invalid
        expect(dns_record.errors[:ip]).to eq(['is invalid', 'can\'t be blank'])
      end

      it 'should not allow to create with invalid ip' do
        dns_record = build(:dns_record, ip: 'invalid ip')

        expect { dns_record.save! }.to raise_error(ActiveRecord::RecordInvalid)

        expect(dns_record.errors[:ip]).not_to be_nil
      end
    end

    it { should validate_presence_of(:ip) }
  end

  describe 'associations' do
    it { should have_and_belong_to_many(:hostnames) }
  end

  describe 'database constraints', database: true do
    context 'ip' do
      it 'must have a NOT NULL constraint' do
        test_column_not_null(:ip)
        test_column_not_null_message(:ip)
      end

      context 'INET type' do
        # Here it's necessary to execute the query directly on the database because ActiveRecord
        # will try to cast `ip` to a `IPAddr` when using its API
        it 'should not allow to insert an invalid IPV4 value in ip' do
          insert_query = "INSERT INTO dns_records (ip) VALUES ('invalid_ip')"

          expect do
            DnsRecord.connection.execute(insert_query)
          end.to raise_error(ActiveRecord::StatementInvalid, /PG::InvalidTextRepresentation: ERROR:  invalid input syntax for type inet: "invalid_ip"/)
        end
      end
    end
  end

  describe 'database indexes', database: true do
    context 'ip' do
      it { should have_db_index(:ip) }
    end
  end
end
