# == Schema Information
#
# Table name: hostnames
#
#  id         :bigint           not null, primary key
#  label      :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_hostnames_on_label  (label) UNIQUE
#
require 'rails_helper'

RSpec.describe Hostname, type: :model do
  alias_method :build_hostname, :build_model
  alias_method :create_hostname_invalidating, :create_model_invalidating

  def test_factory
    :hostname
  end

  describe 'constants' do
  end

  describe 'model API' do
    context 'label' do
      context 'valid domain name' do
        it 'creates a new Hostname' do
          expect { create(:hostname, label: 'fidelis.work') }.to change(Hostname, :count).by(1)
        end

        it 'should persist with numeric label' do
          expect { create(:hostname, label: '123.work') }.to change(Hostname, :count).by(1)
        end
      end

      context 'invalid' do
        it 'should validate' do
          hostname = build(:hostname, label: SecureRandom.hex)

          expect(hostname).not_to be_valid
          expect(hostname.errors[:label]).to eq(['is not a fully qualified domain name'])
        end

        it 'should raise Exception' do
          expect do
            create(:hostname, label: SecureRandom.hex)
          end.to raise_error(ActiveRecord::RecordInvalid, /Validation failed: Label is not a fully qualified domain name/)
        end

        it 'should raise Exception when domain name is without TLD' do
          expect do
            create(:hostname, label: 'fidelis')
          end.to raise_error(ActiveRecord::RecordInvalid, /Validation failed: Label is not a fully qualified domain name/)
        end
      end
    end
  end

  describe 'model validations' do
    context 'label' do
      context 'uniqueness' do
        subject { build(:hostname) }

        it { should validate_uniqueness_of(:label) }
      end

      context 'valid domain name' do
        it 'should be valid' do
          expect(build(:hostname, label: 'fidelis.work')).to be_valid
        end

        it 'should be valid with numeric label' do
          expect(build(:hostname, label: '123.work')).to be_valid
        end
      end

      context 'invalid' do
        context 'presence' do
          it 'should validate' do
            hostname = build(:hostname, label: '')

            expect(hostname).not_to be_valid
            expect(hostname.errors[:label]).to eq(['must be between 1 and 255 characters long', 'is not a fully qualified domain name'])
          end

          it 'should validate #2' do
            hostname = build(:hostname, label: '      ')

            expect(hostname).not_to be_valid
            expect(hostname.errors[:label]).to eq(['contains invalid characters (valid characters: [a-z0-9\\-])', 'is not a fully qualified domain name'])
          end
        end

        it 'should validate' do
          hostname = build(:hostname, label: SecureRandom.hex)

          expect(hostname).not_to be_valid
          expect(hostname.errors[:label]).to eq(['is not a fully qualified domain name'])
        end

        it 'should raise Exception' do
          expect(build(:hostname, label: SecureRandom.hex)).not_to be_valid
        end

        it 'should raise Exception when domain name is without TLD' do
          expect(build(:hostname, label: 'fidelis')).not_to be_valid
        end
      end
    end
  end

  describe 'associations' do
    it { should have_and_belong_to_many(:dns_records) }
  end

  describe 'database constraints', database: true do
    context 'label' do
      it 'must have a NOT NULL constraint' do
        test_column_not_null(:label)
        test_column_not_null_message(:label)
      end
    end
  end

  describe 'database indexes', database: true do
    context 'label' do
      it { should have_db_index(:label) }

      context 'UNIQUE indexes' do
        let(:existent_record) { create(:hostname) }

        it { should have_db_index(:label).unique }

        it 'must have a UNIQUE constraint' do
          test_column_not_duplicated(:label)
          test_column_not_duplicated_message(:label)
        end
      end
    end
  end
end
