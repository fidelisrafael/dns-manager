class DnsRecordsController < ApplicationController
  include DnsRecordsControllerHelpers

  # GET /dns_records
  def index
    service = DnsRecordGetAllService.new(dns_record_get_all_service_params)

    render json: serialize_index_response(service)
  end

  # POST /dns_records
  def create
    service = DnsRecordCreateService.new(dns_record_create_service_params)
    service = execute_service(service)

    if service.success?
      render json: serialize_create_response(service), status: service.response_status, location: service.record
    else
      render json: serialize_create_errors_response(service), status: service.response_status_code
    end
  end
end
