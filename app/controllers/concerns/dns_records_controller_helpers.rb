module DnsRecordsControllerHelpers
  ### Index Helpers

  def dns_record_get_all_service_params
    params.permit!
  end

  def serialize_index_response(service, each_serializer = SimpleDnsRecordSerializer, options = {})
    response = super(service, each_serializer, options.merge(only: %i[id ip]))
    response[:related_records] = service.serialized_related_hostnames_records

    response
  end

  def serialize_index_records(collection, each_serializer = SimpleDnsRecordSerializer, options = {})
    serialize_dns_records(collection, each_serializer, options)
  end

  def serialize_dns_records(dns_records, each_serializer = SimpleDnsRecordSerializer, options = {})
    ActiveModel::ArraySerializer.new(dns_records, options.merge(each_serializer: each_serializer))
  end

  ### Create helpers

  def serialize_create_response(service, options = {})
    simple_serialize_dns_record(service.record, options.merge(only: %i[id]))
  end

  def serialize_create_errors_response(service, _options = {})
    {
      errors: service.errors,
      status: service.response_status,
      status_code: service.response_status_code
    }
  end

  def simple_serialize_dns_record(dns_record, options = {})
    SimpleDnsRecordSerializer.new(dns_record, options)
  end

  def dns_record_params
    params.require(:dns_record).permit(:ip, hostnames_attributes: [:label])
  end

  def dns_record_create_service_params
    { dns_record: dns_record_params }
  end
end
