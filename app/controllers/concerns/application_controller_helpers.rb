module ApplicationControllerHelpers
  include Pagy::Backend

  def serialize_index_response(service, each_serializer = SimpleDnsRecordSerializer, options = {})
    execute_service(service)

    records = service.records
    pagination = service.pagination

    pagy_metadata = pagy_metadata(pagination, true)
    pagination_metadata = serialize_pagination_metadata(pagy_metadata)

    serialized_records = serialize_index_records(records, each_serializer, options)

    serialize_final_index_response(serialized_records, pagination_metadata)
  end

  def serialize_final_index_response(serialized_records, pagination_metadata)
    {
      records: serialized_records,
      total_records: pagination_metadata[:pagination][:items]
    }.merge(serialize_metadata(pagination_metadata))
  end

  def serialize_pagination_metadata(pagination_metadata, except = %i[vars series])
    { pagination: pagination_metadata.except(*except) }
  end

  def serialize_metadata(data)
    {
      metadata: data
    }
  end

  def execute_service(service)
    service.execute unless service.executed?

    service
  end

  # :nocov:
  def serialize_index_records(*_args)
    raise NotImplementedError, "The method \"#{__method__}\" should be implemented in \"#{self.class}\""
  end
end
