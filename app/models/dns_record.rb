# == Schema Information
#
# Table name: dns_records
#
#  id         :bigint           not null, primary key
#  ip         :inet             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_dns_records_on_ip  (ip)
#
require 'resolv'

class DnsRecord < ApplicationRecord
  # This allow to validate both IPV4 and IPV6
  IP_ADDRESS_REGEXP = Regexp.union(Resolv::IPv4::Regex, Resolv::IPv6::Regex)

  validates :ip, format: { with: IP_ADDRESS_REGEXP }, presence: true

  has_and_belongs_to_many :hostnames

  accepts_nested_attributes_for :hostnames

  def hostnames_attributes=(attributes)
    attributes.each do |hostname_attributes|
      # Necessary so we don't try to create duplicate hostname entries
      hostname = Hostname.find_or_create_by(hostname_attributes)

      hostname.dns_records << self if valid?
    end
  end
end
