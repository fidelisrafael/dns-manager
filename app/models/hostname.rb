# == Schema Information
#
# Table name: hostnames
#
#  id         :bigint           not null, primary key
#  label      :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_hostnames_on_label  (label) UNIQUE
#
class Hostname < ApplicationRecord
  has_and_belongs_to_many :dns_records

  validates :label, domainname: true, uniqueness: true
end
