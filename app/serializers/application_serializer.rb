class ApplicationSerializer < ActiveModel::Serializer
  root false

  protected

  # Override to normalize all serialized dates through serializers
  def attributes
    super.transform_values do |value|
      value = value.iso8601 if value.respond_to?(:iso8601)
      value
    end
  end
end
