# == Schema Information
#
# Table name: dns_records
#
#  id         :bigint           not null, primary key
#  ip         :inet             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_dns_records_on_ip  (ip)
#
class SimpleDnsRecordSerializer < ApplicationSerializer
  attributes :id, :ip, :created_at, :updated_at

  private

  def ip
    object.ip.to_s
  end
end
