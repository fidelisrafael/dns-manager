# == Schema Information
#
# Table name: hostnames
#
#  id         :bigint           not null, primary key
#  label      :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_hostnames_on_label  (label) UNIQUE
#
class SimpleHostnameSerializer < ApplicationSerializer
  attributes :id, :label, :created_at, :updated_at
end
