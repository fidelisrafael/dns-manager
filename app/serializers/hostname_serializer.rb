# == Schema Information
#
# Table name: hostnames
#
#  id         :bigint           not null, primary key
#  label      :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_hostnames_on_label  (label) UNIQUE
#
class HostnameSerializer < SimpleHostnameSerializer
  has_many :dns_records, serializer: SimpleDnsRecordSerializer
end
