class DnsRecordCreateService < NiftyServices::BaseCreateService
  record_type DnsRecord

  WHITELIST_ATTRIBUTES = %i[ip hostnames_attributes].freeze

  whitelist_attributes WHITELIST_ATTRIBUTES

  def record_attributes_hash
    (@options[:dns_record] || @options) .to_h
  end

  # TODO: Validate ACL
  def can_create_record?
    true
  end
end
