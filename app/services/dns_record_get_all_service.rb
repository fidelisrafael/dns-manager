class DnsRecordGetAllService < NiftyServices::BaseActionService
  include Pagy::Backend

  action_name :get_all_paginated_and_filtered_dns_record

  attr_reader :records, :pagination

  def initialize(*args)
    # To maintain an standard API
    @records = DnsRecord.none

    super(*args)
  end

  def execute
    execute_action do
      @pagination, @records = fetch_paginated_records

      success_response
    end
  end

  def included_hostnames
    fetch_array_param(:included, @options)
  end

  def excluded_hostnames
    fetch_array_param(:excluded, @options)
  end

  def only_include_filter?
    included_hostnames.present? && excluded_hostnames.blank?
  end

  def only_exclude_filter?
    included_hostnames.blank? && excluded_hostnames.present?
  end

  def all_filters?
    included_hostnames.present? && excluded_hostnames.present?
  end

  def related_hostnames_records
    return [] unless executed?

    to_exclude = excluded_hostnames + included_hostnames
    hostnames = @records.flat_map(&:hostnames)
    hostnames.reject { |hostname| to_exclude.member?(hostname.label) }
  end

  def serialized_related_hostnames_records
    serialize_related_hostnames_records(related_hostnames_records)
  end

  def record_error_key
    :services
  end

  private

  def serialize_related_hostnames_records(related_records)
    related_records.group_by(&:label).map do |hostname, items|
      { hostname: hostname, count: items.size }
    end
  end

  def aggreate_dns_records_query
    DnsRecord
      .select('dns_records.id', 'dns_records.ip')
      .select('array_agg(hostnames.label) labels')
      .joins(:hostnames)
      .group('dns_records.id')
  end

  def fetch_records
    relation = DnsRecord.from(aggreate_dns_records_query, :dns_records)

    apply_includes(apply_filters(relation))
  end

  def fetch_paginated_records
    relation = fetch_records

    pagy(relation, pagination_params.merge(options))
  end

  def apply_includes(relation)
    relation.includes(:hostnames)
  end

  def apply_all_filters(relation)
    relation = apply_excluded_filter(relation)
    relation = apply_included_filter(relation)

    relation
  end

  def apply_excluded_filter(relation)
    relation.where.not('labels @> ?', to_pg_array(excluded_hostnames))
  end

  def apply_included_filter(relation)
    relation.where('labels @> ?', to_pg_array(included_hostnames))
  end

  def apply_filters(relation)
    return apply_all_filters(relation) if all_filters?
    return apply_excluded_filter(relation) if only_exclude_filter?
    return apply_included_filter(relation) if only_include_filter?

    relation
  end

  # For Pagy
  def params
    @options[:pagy_params] || @options
  end

  def pagination_params
    {
      items: params[:per_page],
      page: params[:page]
    }
  end

  def fetch_array_param(key, params = {})
    Array.wrap(params.fetch(key, []))
  end

  def to_pg_array(value)
    PG::TextEncoder::Array.new.encode(Array.wrap(value))
  end

  # TODO: Validate ACL
  def can_execute_action?
    true
  end
end
