#!/bin/bash
set -e

ruby -v

bundle -v

gem install bundler -v $(cat Gemfile.lock | tail -1 | tr -d " ")

bundle -v

bundle _2.1.4_ exec rake db:migrate

rm -f /usr/src/app/tmp/pids/server.pid

bundle _2.1.4_ exec rails server -b 0.0.0.0
