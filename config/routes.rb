# == Route Map
#
#                                Prefix Verb   URI Pattern                                                                              Controller#Action
#                           dns_records GET    /dns_records(.:format)                                                                   dns_records#index
#                                       POST   /dns_records(.:format)                                                                   dns_records#create
#                            dns_record GET    /dns_records/:id(.:format)                                                               dns_records#show

Rails.application.routes.draw do
  resources :dns_records, except: [:destroy, :update, :new]
end
