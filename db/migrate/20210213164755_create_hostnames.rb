class CreateHostnames < ActiveRecord::Migration[6.0]
  def change
    create_table :hostnames do |t|
      t.string :label, null: false

      t.timestamps
    end

    add_index :hostnames, :label, unique: true
  end
end
