class CreateDnsRecords < ActiveRecord::Migration[6.0]
  def change
    create_table :dns_records do |t|
      t.inet :ip, null: false, index: true

      t.timestamps
    end
  end
end
