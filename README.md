# DNS Manager

<div style="background-color: white; text-align: center; border-radius: 20px; padding: 10px;">

![Logo DNS Manager](./public/dns-logo.png)

</div>

---

## Master (Production):

[![pipeline status](https://gitlab.com/fidelisrafael/dns-manager/badges/master/pipeline.svg)](https://gitlab.com/fidelisrafael/dns-manager/-/pipelines?page=1&scope=all&ref=master)

[![coverage report](https://gitlab.com/fidelisrafael/dns-manager/badges/master/coverage.svg)](https://gitlab.com/fidelisrafael/dns-manager/-/tree/master)

## Develop (Homolog):

[![pipeline status](https://gitlab.com/fidelisrafael/dns-manager/badges/develop/pipeline.svg)](https://gitlab.com/fidelisrafael/dns-manager/-/pipelines?page=1&scope=all&ref=develop)

[![coverage report](https://gitlab.com/fidelisrafael/dns-manager/badges/develop/coverage.svg)](https://gitlab.com/fidelisrafael/dns-manager/-/tree/master)

---

## About

This is a simple DNS manager HTTP API application to handle DNS and Hostnames management.

---

## Requirements

- [Ruby 2.7.1](https://www.ruby-lang.org/pt/downloads/)
- [PostgreSQL >= 12](https://www.postgresql.org/download/)

---

## Stack

- [Ruby on Rails (6.0.3.5)](https://rubygems.org/gems/rails/versions/6.0.3.5)
- [Active Model Serializers (0.9.0)](https://github.com/rails-api/active_model_serializers/tree/0-9-stable)
- [Puma (4.3.7)](https://github.com/puma/puma)
- [Sentry (3.0.0)](https://github.com/getsentry/raven-ruby)
- [Rubocop (0.49.1)](https://github.com/rubocop-hq/rubocop)
- [SimpleCov](https://github.com/colszowka/simplecov)
- [RSpec](https://github.com/rspec/rspec)
- [RSpec Rails](https://github.com/rspec/rspec-rails)
- [FactoryBot](https://github.com/thoughtbot/factory_bot)
- [Faker](https://github.com/faker-ruby/faker/)
- [DatabaseCleaner](https://github.com/DatabaseCleaner/database_cleaner)
- [Shoulda Matchers](https://github.com/thoughtbot/shoulda-matchers)
- [Overcommit](https://github.com/sds/overcommit)
- [validates_hostname](https://github.com/KimNorgaard/validates_hostname)
- [Pagy](https://github.com/ddnexus/pagy/)
- [Annotate](https://github.com/ctran/annotate_models)
- [Nifty Services](https://github.com/fidelisrafael/nifty_services)

---

## Development setup

## Setup with Docker + Docker Compose

The simplest way to run this application is by using Docker and docker-compose:

```sh
$ git clone git@gitlab.com:fidelisrafael/dns-manager.git
$ cd dns-manager/
$ docker-compose up
```

---

### Overcommit (Git Hooks)

We use [`overcommit`](https://github.com/sds/overcommit) to guarantee that all `git hooks` will be executed during development with _git_.
For example: To guarantee that the code has no offenses **before committing** or run all tests locally **before sending** the code to the remote repository.

Firstly, make sure you have [`overcommit`](https://github.com/sds/overcommit) correctly installed on your machine.

```sh
$ overcommit --version
# overcommit 0.55.0
```

Install the hooks configurated in `.overcommit.yml` by running:

```sh
$ overcommit --install
# Installing hooks into /(...)/dns-manager/api
# Successfully installed hooks into /(...)/dns-manager/api

$ overcommit --run
overcommit --run
Running pre-commit hooks
Check for trailing whitespace....................[TrailingWhitespace] OK
Check for local paths in Gemfile................[LocalPathsInGemfile] OK
Check YAML syntax........................................[YamlSyntax] OK
Check Gemfile dependencies..............................[BundleCheck] OK
Analyze with RuboCop........................................[RuboCop] OK

✓ All pre-commit hooks passed
```

**OBS:** You can disable Overcommit when needed using:

```sh
SKIP=RSpec git push (...)
```

**OBS 2:** Check it out the file `.overcommit` for more information or run `$ overcommit --help` for more details.

---

## Serve the HTTP application

After migrating the database, you can serve the local application by running:

```sh
$ bundle exec rails s -b 127.0.0.1

=> Booting Puma
=> Rails 6.0.3.5 application starting in development
=> Run `rails server --help` for more startup options
Puma starting in single mode...
* Version 4.3.7 (ruby 2.7.1-p83), codename: Mysterious Traveller
* Min threads: 5, max threads: 5
* Environment: development
* Listening on tcp://127.0.0.1:3000
Use Ctrl-C to stop
```

Access it at: `127.0.0.1:3000`

---

## Manual HTTP API tests

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/ae695ed9671cd6636200)

Just to make sure your setup is up and running properly, you can try to create a `DnsRecord` with `cURL`:

### Create DNSRecord

```sh
$ curl --location --request POST 'http://localhost:3000/dns_records' -H 'Content-Type: application/json' -d '{
  "dns_record": {
    "ip": "1.1.1.1",
    "hostnames_attributes": [
      {
        "label": "lorem.com"
      },
      {
        "label": "ipsum.com"
      },
      {
        "label": "dolor.com"
      },
      {
        "label": "amet.com"
      }
    ]
  }
}'

< HTTP/1.1 201 Created
< X-Frame-Options: SAMEORIGIN
< X-XSS-Protection: 1; mode=block
< X-Content-Type-Options: nosniff
< X-Download-Options: noopen
< X-Permitted-Cross-Domain-Policies: none
< Referrer-Policy: strict-origin-when-cross-origin
< Location: http://localhost:3000/dns_records/1
< Content-Type: application/json; charset=utf-8
< ETag: W/"9e7e65453739bbc64ba155eed18a37bd"
< Cache-Control: max-age=0, private, must-revalidate
< X-Request-Id: ddd6aae1-092c-40c0-9200-9fc0e0d94ebd
< X-Runtime: 0.073221
< Transfer-Encoding: chunked
<

{"id":1}
```

### List DNSRecords

```sh
$ curl --location -H 'Content-Type: application/json' 'http://localhost:3000/dns_records' | python -m json.tool

{
  "records": [
    {
      "id": 1,
      "ip": "1.1.1.1"
    }
  ],
  "total_records": 1,
  "metadata": {
    "pagination": {
      "scaffold_url": "http://localhost:3000/dns_records?page=__pagy_page__",
      "first_url": "http://localhost:3000/dns_records?page=1",
      "prev_url": "http://localhost:3000/dns_records?page=",
      "page_url": "http://localhost:3000/dns_records?page=1",
      "next_url": "http://localhost:3000/dns_records?page=",
      "last_url": "http://localhost:3000/dns_records?page=1",
      "count": 1,
      "page": 1,
      "items": 1,
      "pages": 1,
      "last": 1,
      "from": 1,
      "to": 1,
      "prev": null,
      "next": null
    }
  },
  "related_records": [
    {
        "hostname": "lorem.com",
        "count": 1
    },
    {
        "hostname": "ipsum.com",
        "count": 1
    },
    {
        "hostname": "dolor.com",
        "count": 1
    },
    {
        "hostname": "amet.com",
        "count": 1
    }
  ]
}
```

---

## Deploy

[TODO]

---

## Tests suite

We're using [`RSpec`](https://rspec.info/) + [`rspec-rails`](https://github.com/rspec/rspec-rails) to test this application;

You can also use the `web` application on `docker-compose` to run the test suite of this application:

```sh
$ docker-compose run web bundle exec rails db:setup
$ docker-compose run web bundle exec rspec

# Running:

..............

Finished in 4.39 seconds (files took 2.07 seconds to load)
107 examples, 0 failures

Coverage report generated for RSpec to /(...)/dns-manager/coverage. 161 / 161 LOC (100.0%) covered.
```

---

## Code Coverage

We use [`SimpleCov`](https://github.com/colszowka/simplecov) to generate and instrument this application code coverage report when the tests are run.

When you run `bundle exec rails spec` the metadata + HTML report of code coverage are created in the `coverage/` folder, to check it out you can run:

```sh
$ xdg-open coverage/index.html   # Linux
$ open coverage/index.html       # Mac
$ start "" "coverage\index.html" # Windows
```

Code Coverage report hosted at Gitlab Pages: https://fidelisrafael.gitlab.io/dns-manager/d2b11db4b3/coverage/

---

## F.A.Q

---

## References & useful links

- https://icons8.com.br/icon/41400/dns (DNS Icon ==> Free)

- https://www.postgresql.org/docs/9.1/datatype-net-types.html

- https://guides.rubyonrails.org/active_record_postgresql.html#network-address-types

- https://ruby-doc.org/stdlib-2.5.0/libdoc/ipaddr/rdoc/IPAddr.html

- https://gitlab.com/fidelisrafael/dns-manager

- https://guides.rubyonrails.org/

- https://guides.rubyonrails.org/testing.html

- https://fidelisrafael.gitlab.io/dns-manager/d2b11db4b3/coverage

- https://github.com/rspec/rspec

- https://github.com/thoughtbot/factory_bot

- https://github.com/faker-ruby/faker/

- https://github.com/faker-ruby/faker/blob/master/doc/default/internet.md

- https://github.com/DatabaseCleaner/database_cleaner

- https://github.com/thoughtbot/shoulda-matchers

- https://github.com/sds/overcommit

- https://github.com/KimNorgaard/validates_hostname

- https://github.com/fidelisrafael/nifty_services/blob/master/docs/api.md#full-public-api-methods-list

- https://github.com/ddnexus/pagy/

- https://www.oreilly.com/library/view/regular-expressions-cookbook/9781449327453/ch08s15.html

- https://man7.org/linux/man-pages/man7/hostname.7.html

- https://www.wikiwand.com/en/Domain_Name_System

- https://www.wikiwand.com/en/Hostname
