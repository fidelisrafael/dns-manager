FROM ruby:2.7.1
LABEL maintainer="Rafael Fidelis"

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY . /usr/src/app

RUN chmod -R a+w /usr/src/app/log

RUN ruby -v

RUN bundle -v

RUN gem install bundler -v $(cat Gemfile.lock | tail -1 | tr -d " ")

RUN bundle -v

RUN bundle _2.1.4_ install

EXPOSE 3000

CMD ["./start.sh"]
